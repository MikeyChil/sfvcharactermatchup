package com.mbc.streetfightervcharactermatchup

import android.content.Context
import android.util.Log
import org.json.JSONObject
import java.io.File
import java.io.FileInputStream

/**
 * Created by Michael Childs on 3/03/2018.
 */
class FrameData {

    var characters:MutableList<Character> = mutableListOf<Character>()
    private var populated:Boolean = false
    private val context:Context

    constructor(contextParam:Context) {
        context = contextParam
    }

    fun isPopulated():Boolean {
        return populated
    }

    private fun read():JSONObject?
    {
        var fileName = context.getString(R.string.fileName)

        var file:File = File(context.filesDir, fileName)

        if(file.exists() == true) {

            var fileInput: FileInputStream = context.openFileInput(fileName)

            var size: Int = fileInput.available()
            if (size > 0) {
                var buffer: ByteArray = ByteArray(size)

                fileInput.read(buffer)
                fileInput.close()

                var jsonString: String = String(buffer)
                var jsonObject: JSONObject = JSONObject(jsonString)

                return jsonObject
            }
        }

        return null
    }

    fun build() {

        var json:JSONObject? = read()

        if(json == null) {
            return
        }

        characters.clear()

        val keys:Iterator<String> = json.keys()

        while(keys.hasNext()) {
            var characterKey:String = keys.next()
            var charJSON:JSONObject? = json.get(characterKey) as JSONObject
            if(charJSON != null) {
                var character:Character = Character(characterKey);
                characters.add(character)
                var movesJSON:JSONObject = charJSON.get("moves") as JSONObject
                var normalMovesJSON:JSONObject = movesJSON.get("normal") as JSONObject
                var vtOneMovesJSON:JSONObject = movesJSON.get("vtOne") as JSONObject
                var vtTwoMovesJSON:JSONObject = movesJSON.get("vtTwo") as JSONObject

                parseNormalMoves(normalMovesJSON, character)
            }
        }

        populated = true
    }

    fun getCharacter(name:String):Character? {

        for(char:Character in characters) {
            if(char.name == name) {
                return char
            }
        }
        return null
    }

    private fun parseNormalMoves(normalMovesJSON:JSONObject, character:Character) {

        val normalMoveKeys:Iterator<String> = normalMovesJSON.keys()
        while(normalMoveKeys.hasNext())
        {
            var moveObject:Move = Move()
            moveObject.name = normalMoveKeys.next()
            var moveJSON:JSONObject = normalMovesJSON.get(moveObject.name) as JSONObject

            if(shouldAddMove(moveJSON)) {
                moveObject.advantage = moveJSON.getInt("onBlock")
                moveObject.startUp = parseStartUp(moveJSON)
                moveObject.input = moveJSON.getString("plnCmd")
                moveObject.type = moveJSON.getString("moveType")

                character.moves.put(moveObject.name, moveObject)
            } else {
                Log.d("FramaData", moveObject.name + " does not have required data")
            }
        }
    }

    private fun shouldAddMove(moveJSON: JSONObject):Boolean {

        if(moveJSON.has("onBlock") == false) return false
        if(moveJSON.has("startup") == false) return false
        if(moveJSON.has("plnCmd") == false) return false
        if(Utils.isInt(moveJSON.getString(("onBlock"))) == false) return false
        if(Utils.isInt(moveJSON.getString(("startup"))) == false) return false

        return true
    }

    private fun parseStartUp(moveJSON:JSONObject):Int {

        var startup = moveJSON.getString("startup")
        if(startup.contains("+")) {
            var values = startup.split("+")
            return tallyTotalStartUpFrames(values)
        } else if (startup.contains("*")) {
            var values = startup.split("*")
            return tallyTotalStartUpFrames(values)
        } else {
            return startup.toInt()
        }
    }

    private fun tallyTotalStartUpFrames(values:List<String>):Int {

        var total:Int = 0
        for(value:String in values) {
            if (Utils.isInt(value)) {
                total += value.toInt()
            }
        }
        return total
    }


}