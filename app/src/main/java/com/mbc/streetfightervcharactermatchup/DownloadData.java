package com.mbc.streetfightervcharactermatchup;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Debug;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;
import org.xml.sax.Parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.transform.Result;

/**
 * Created by Michael Childs on 2/03/2018.
 *
 * Attempts to keep the frame data up to date by downloading the data provided by the
 * creators of the frame data app "FAT" https://fullmeter.com/fat/
 * raw data at https://raw.githubusercontent.com/D4RKONION/fatsfvframedatajson/master/sfv.json
 *
 */

public class DownloadData extends AsyncTask<String, Integer, String> {

    private Context context;
    private MainActivity activity;

    InputStream input = null;
    OutputStream output = null;
    HttpURLConnection connection = null;

    public DownloadData(Context contextParam, MainActivity activityParam) {

        context = contextParam;
        activity = activityParam;
    }

    @Override
    protected String doInBackground(String... unused) {

        try {
            URL url = new URL("https://raw.githubusercontent.com/D4RKONION/fatsfvframedatajson/master/sfv.json");
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                LogError(connection.getResponseCode() + " " +
                        connection.getResponseMessage());
                return "";
            }

            input = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            StringBuilder builder = new StringBuilder();
            String line;

            while((line = reader.readLine()) != null) {
                builder.append(line);
            }

            String fileName = context.getString(R.string.fileName);
            File file = new File(context.getFilesDir(), fileName);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.append(builder);
            writer.close();

            return "";
        } catch (Exception e) {
            LogError(e.getMessage());
            return "";
        }
    }

    void LogError(String errorMessage) {

        Log.e("DownloadData", errorMessage);
    }
}
