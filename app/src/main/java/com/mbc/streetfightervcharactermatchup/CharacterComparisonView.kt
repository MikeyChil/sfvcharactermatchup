package com.mbc.streetfightervcharactermatchup

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.TextView

/**
 * Created by Michael Childs on 4/03/2018.
 */
class CharacterComparisonView {

    private var activity:MainActivity
    private var context:Context

    private var playerCompareLayout:RelativeLayout? = null
    private var movesAlreadyAdded:ArrayList<Move> = ArrayList()

    constructor(contextParam: Context, activityParam: MainActivity) {

        activity = activityParam
        context = contextParam
    }

    public fun init() {

        playerCompareLayout = RelativeLayout(activity)

        var matchParentWidthHeight: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT)
        playerCompareLayout!!.layoutParams = matchParentWidthHeight
    }

    public fun show(frameDataComp:FrameDataComparer){

        activity!!.setContentView(playerCompareLayout)

        movesAlreadyAdded.clear()

        var matchParentWidthHeight: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT)

        playerCompareLayout!!.removeAllViews()

        var scrollView: ScrollView = ScrollView(activity)
        scrollView.layoutParams = matchParentWidthHeight
        playerCompareLayout!!.addView(scrollView)

        var linearLayout: LinearLayout = LinearLayout(activity)
        linearLayout.layoutParams = matchParentWidthHeight
        linearLayout.orientation =  LinearLayout.VERTICAL
        scrollView.addView(linearLayout)

        var textView:TextView = TextView(context)
        textView.text = "Range permitting, " + frameDataComp.player.name + " can punish the following moves from " + frameDataComp.opponent.name + "."
        textView.textSize = 20.0f
        textView.gravity = Gravity.CENTER
        textView.setTextColor(Color.GRAY)
        linearLayout.addView(textView)

        addPunishableMovesToLayout(frameDataComp.opponentHeavyPunishableMoves, "Heavy Punishes", linearLayout)
        addPunishableMovesToLayout(frameDataComp.opponentMediumPunishableMoves, "Medium Punishes", linearLayout)
        addPunishableMovesToLayout(frameDataComp.opponentLightPunishableMoves, "Light Punishes", linearLayout)

    }

    private fun addPunishableMovesToLayout(punishableMoves:ArrayList<Move>, message:String, linearLayout:LinearLayout) {
        var headlineLayoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)
        headlineLayoutParams.setMargins(4,2,4,2)
        var textLayoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)
        textLayoutParams.setMargins(4,0,4,0)

        var textView:TextView = TextView(context)

        textView.layoutParams = headlineLayoutParams
        textView.text = message
        textView.textSize = 26.0f
        textView.gravity = Gravity.CENTER
        textView.setTextColor(Color.BLACK)
        textView.setBackgroundColor(Color.LTGRAY)
        linearLayout.addView(textView)

        for(move:Move in punishableMoves) {

            var used:Boolean = false
            for(usedMoves:Move in movesAlreadyAdded) {
                if(usedMoves.name == move.name) {
                    used = true
                }
            }

            if( !used ) {

                movesAlreadyAdded.add(move)

                var textView: TextView = TextView(context)
                textView.layoutParams = textLayoutParams
                textView.text = move.advantage.toString() + " : " + move.name + " (" + move.input + ")"
                textView.textSize = 20.0f
                textView.gravity = Gravity.LEFT
                textView.setTextColor(Color.DKGRAY)
                linearLayout.addView(textView)
            }
        }
    }
}