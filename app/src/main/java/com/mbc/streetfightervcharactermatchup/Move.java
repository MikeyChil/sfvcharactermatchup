package com.mbc.streetfightervcharactermatchup;

/**
 * Created by Michael Childs on 3/03/2018.
 */

public class Move {
    public String name = "";
    public String input = "";
    public String type = "";
    public int startUp = -1;
    public int advantage = -1;

    public Move() {

    }

    public Move(String moveName, String moveInput, String moveType, int moveStartUp, int moveAdvantage) {
        name = moveName;
        input = moveInput;
        type = moveType;
        startUp = moveStartUp;
        advantage = moveAdvantage;
    }

    public void copy(Move other) {
        name = other.name;
        input = other.input;
        type = other.type;
        startUp = other.startUp;
        advantage = other.advantage;
    }
}
