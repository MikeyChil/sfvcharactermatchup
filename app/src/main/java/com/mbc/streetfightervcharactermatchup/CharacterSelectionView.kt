package com.mbc.streetfightervcharactermatchup

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.*

/**
 * Created by Michael Childs on 4/03/2018.
 */
class CharacterSelectionView {

    private var activity:MainActivity? = null
    private var context:Context? = null
    private var frameData:FrameData? = null

    private var download:DownloadData? = null
    private var selectedPlayer:Character = Character("")
    private var selectedOpponent:Character = Character("")
    private var playerSelect: RelativeLayout? = null
    private var updateButton:Button? = null

    private val STATE_SELECT_PLAYER:Int = 0
    private val STATE_SELECT_OPPONENT:Int = 1
    private var state:Int = STATE_SELECT_PLAYER


    constructor(contextParam:Context, activityParam: MainActivity, frameDataParam:FrameData) {
        activity = activityParam
        context = contextParam
        frameData = frameDataParam
    }

    private val playerSelectListener = View.OnClickListener {
        var button:Button = it as Button
        var characterName:String = button.text.toString()

        for(character:Character in frameData!!.characters) {
            if(character.name == characterName) {

                if(state == STATE_SELECT_PLAYER) {
                    selectedPlayer = character
                    state = STATE_SELECT_OPPONENT
                    updateButton!!.text = context!!.getString(R.string.selectOpponent)
                }
                else if(state == STATE_SELECT_OPPONENT) {
                    selectedOpponent = character
                    activity!!.showCharacterComparisonView(selectedPlayer, selectedOpponent)
                }
                val toast = Toast.makeText(activity!!.applicationContext, characterName + " selected!", Toast.LENGTH_LONG)
                toast.show()
                break
            }
        }
    }

    private val updateDataListener = View.OnClickListener {
        if(download == null) {
            download = DownloadData(activity!!.getApplicationContext(), activity)
            download?.execute()

            val toast = Toast.makeText(activity!!.getApplicationContext(), "Checking for updates!", Toast.LENGTH_LONG)
            toast.show()
        }
    }

    public fun init() {

        playerSelect = RelativeLayout(activity)
        var matchParentWidthHeight: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT)
        playerSelect!!.layoutParams = matchParentWidthHeight


        var playerScrollView: ScrollView = ScrollView(activity)
        playerScrollView.layoutParams = matchParentWidthHeight
        playerSelect!!.addView(playerScrollView)

        var buttonLinearLayout: LinearLayout = LinearLayout(activity)
        buttonLinearLayout.layoutParams = matchParentWidthHeight
        buttonLinearLayout.orientation =  LinearLayout.VERTICAL
        playerScrollView.addView(buttonLinearLayout)

        var characterButtonLayoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)

        characterButtonLayoutParams.gravity = Gravity.CENTER_HORIZONTAL
        characterButtonLayoutParams.setMargins(0,5,0,0)
        characterButtonLayoutParams.height = 100

        updateButton = Button(activity)

        updateButton!!.layoutParams = characterButtonLayoutParams
        updateButton!!.text = context!!.getString(R.string.selectPlayer)
        updateButton!!.textSize = 22.0f
        updateButton!!.setOnClickListener(updateDataListener)
        buttonLinearLayout.addView(updateButton)

        characterButtonLayoutParams.height = 150

        for(character:Character in frameData!!.characters) {
            var button: Button = Button(activity)
            button.layoutParams = characterButtonLayoutParams
            button.text = character.name
            button.textSize = 22.0f
            button.setOnClickListener(playerSelectListener)

            buttonLinearLayout.addView(button)
        }
    }

    public fun show() {
        state = STATE_SELECT_PLAYER
        updateButton!!.text = context!!.getString(R.string.selectPlayer)
        activity!!.setContentView(playerSelect)
    }

    public fun onBackPressed():Boolean {
        if(state == STATE_SELECT_OPPONENT) {
            state = STATE_SELECT_PLAYER
            updateButton!!.text = context!!.getString(R.string.selectPlayer)
            return true
        }
        return false
    }
}