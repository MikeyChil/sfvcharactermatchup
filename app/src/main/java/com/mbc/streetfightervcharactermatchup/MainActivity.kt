package com.mbc.streetfightervcharactermatchup

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import org.json.JSONObject
import java.util.*

class MainActivity : AppCompatActivity() {

    private var characterSelectionView:CharacterSelectionView? = null
    private var characterComparisonView:CharacterComparisonView? = null

    private var lastUpdated:Date? = null
    private var frameData:FrameData? = null
    private var comparer:FrameDataComparer = FrameDataComparer()

    private val STATE_CHARACTER_SELECT:Int = 0
    private val STATE_CHARACTER_COMPARE:Int = 1
    private var state:Int = STATE_CHARACTER_SELECT


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        frameData = FrameData(applicationContext)
        frameData!!.build()

        characterSelectionView = CharacterSelectionView(applicationContext, this, frameData!!)
        characterSelectionView!!.init()
        showCharacterSelectionView()

        characterComparisonView = CharacterComparisonView(applicationContext, this)
        characterComparisonView!!.init()

    }


    override fun onBackPressed() {

        if(state == STATE_CHARACTER_COMPARE) {
            showCharacterSelectionView()
        } else {
            var handled:Boolean = characterSelectionView!!.onBackPressed()

            if(!handled) {
                super.onBackPressed()
            }
        }
    }

    public fun showCharacterSelectionView() {

        state = STATE_CHARACTER_SELECT
        characterSelectionView!!.show()
    }

    public fun showCharacterComparisonView(player:Character, opponent:Character) {

        state = STATE_CHARACTER_COMPARE
        comparer.Compare(player, opponent)
        characterComparisonView!!.show(comparer)

    }

}
