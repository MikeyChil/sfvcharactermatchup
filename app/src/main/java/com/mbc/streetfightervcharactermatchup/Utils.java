package com.mbc.streetfightervcharactermatchup;

/**
 * Created by Michael Childs on 4/03/2018.
 */

public class Utils {
    public static boolean isInt(String string) {
        try{
            int num = Integer.parseInt(string);
        } catch(Exception exception) {
            return false;
        }
        return true;
    }
}
