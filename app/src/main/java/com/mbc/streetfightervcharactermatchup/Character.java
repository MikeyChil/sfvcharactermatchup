package com.mbc.streetfightervcharactermatchup;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Michael Childs on 3/03/2018.
 */

public class Character {
    public String name = "";
    public Map<String, Move> moves = new HashMap<>();

    public Character(String characterName) {

        name = characterName;
    }
}
