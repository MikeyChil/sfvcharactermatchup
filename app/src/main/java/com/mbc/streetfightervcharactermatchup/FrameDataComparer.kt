package com.mbc.streetfightervcharactermatchup

/**
 * Created by Michael Childs on 3/03/2018.
 */
class FrameDataComparer {

    var player:Character = Character("")
    var opponent:Character = Character("")

    var playerFastestLight:Move = Move()
    var playerFastestMedium:Move = Move()
    var playerFastestHeavy:Move = Move()
    var playerFastestSpecial:Move = Move()

    var opponentLightPunishableMoves:ArrayList<Move> = ArrayList<Move>()
    var opponentMediumPunishableMoves:ArrayList<Move> = ArrayList<Move>()
    var opponentHeavyPunishableMoves:ArrayList<Move> = ArrayList<Move>()

    fun Compare(playerChar:Character, opponentChar:Character) {

        player = playerChar
        opponent = opponentChar

        opponentLightPunishableMoves.clear()
        opponentMediumPunishableMoves.clear()
        opponentHeavyPunishableMoves.clear()

        getFastestMoveOfType(playerChar, arrayOf("LK", "LP"), playerFastestLight)
        getFastestMoveOfType(playerChar, arrayOf("MK", "MP"), playerFastestMedium)
        getFastestMoveOfType(playerChar, arrayOf("HK", "HP"), playerFastestHeavy)
        getFastestMoveOfType(playerChar, arrayOf("special"), playerFastestSpecial)

        getOpponentPunishableMoves(playerFastestLight, opponentChar, opponentLightPunishableMoves)
        getOpponentPunishableMoves(playerFastestMedium, opponentChar, opponentMediumPunishableMoves)
        getOpponentPunishableMoves(playerFastestHeavy, opponentChar, opponentHeavyPunishableMoves)

    }

    private fun getOpponentPunishableMoves(playerMove:Move, opponent:Character, punishableMovesList:ArrayList<Move>) {

        for(moveIter in opponent.moves) {
            if(moveIter.value.advantage + playerMove.startUp <= 0) {
                punishableMovesList.add(moveIter.value)
            }
        }
    }

    private fun getFastestMoveOfType(player:Character, types:Array<String>, fastestMove:Move) {

        var movesIterator = player.moves.entries.iterator()
        var fastestTemp:Move = Move()
        fastestTemp.startUp = 9999

        while(movesIterator.hasNext()) {
            var entry = movesIterator.next()

            if(entry.value.type != "normal") {
                continue
            }

            for(type:String in types) {
                if (entry.value.input.contains(type)) {
                    if (entry.value.startUp < fastestTemp.startUp) {
                        fastestTemp.copy(entry.value)
                    }
                }
            }
        }

        fastestMove.copy(fastestTemp)
    }
}